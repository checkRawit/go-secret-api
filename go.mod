module gitlab.com/checkRawit/go-secret-api

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.1.2
	github.com/joho/godotenv v1.3.0
	go.mongodb.org/mongo-driver v1.4.1
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
