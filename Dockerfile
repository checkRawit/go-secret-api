# Image for build exe
FROM golang:1.15 AS builder

WORKDIR /app

# Download dependencies
COPY go.mod go.sum ./
RUN go mod download

COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -v -o server

# Image for run server
FROM alpine:3
RUN apk add --no-cache ca-certificates

WORKDIR /app

COPY --from=builder /app/server ./

CMD [ "./server" ]
