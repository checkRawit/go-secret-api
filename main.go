package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/checkRawit/go-secret-api/app"
)

func main() {
	repository, err := app.NewMongodbSecretRepository(config.DatabaseURI, config.DatabaseName, "secrets")
	if err != nil {
		panic(err)
	}
	defer repository.Close()

	usecase := app.NewUsecase(repository, app.UUIDSecretIDService{}, app.BcryptHashService{})
	secretHandler := NewSecretHandler(usecase)

	router := gin.Default()
	router.POST("/secrets", secretHandler.POSTSecret)
	router.GET("/secrets/:id", secretHandler.GETSecret)

	router.Run(fmt.Sprintf(":%d", config.ServerPort))
}
