package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/checkRawit/go-secret-api/app"
)

// SecretHandler is a handler for Secret Usecase
type SecretHandler struct {
	secretUsecase app.Usecase
}

// NewSecretHandler create a SecretHandler
func NewSecretHandler(secretUsecase app.Usecase) SecretHandler {
	return SecretHandler{secretUsecase}
}

// POSTSecret is a handler for create a secret
func (h SecretHandler) POSTSecret(c *gin.Context) {
	// Parse body
	var body struct {
		ID       string  `json:"id"`
		Message  *string `json:"message"`
		Password string  `json:"password"`
	}
	if err := c.BindJSON(&body); err != nil {
		return
	}

	// Check if message is not in body
	if body.Message == nil {
		c.String(http.StatusBadRequest, "Message is not define")
		return
	}

	// Create secret
	generatedSecretID, err := h.secretUsecase.StoreSecret(*body.Message, body.Password, body.ID)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, gin.H{"id": generatedSecretID})
}

// GETSecret is a handler for get a secret
func (h SecretHandler) GETSecret(c *gin.Context) {
	id := c.Param("id")
	password := c.Query("password")
	message, err := h.secretUsecase.GetSecret(id, password)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": message})
}
