package main

import (
	"log"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

// Config is a struct for app configuration
type Config struct {
	DatabaseURI  string
	DatabaseName string
	ServerPort   int
}

var config Config

func init() {
	// Load .env
	err := godotenv.Load(".env")
	if err != nil {
		log.Println("Skip loading .env file")
	}

	// Config port
	serverPort, err := strconv.Atoi(os.Getenv("PORT"))
	if err != nil {
		// Set default port
		serverPort = 8080
	}

	// Config Database
	databaseURI, DatabaseURIPresent := os.LookupEnv("DATABASE_URI")
	databaseName, DatabaseNamePresent := os.LookupEnv("DATABASE_NAME")
	if !DatabaseNamePresent || !DatabaseURIPresent {
		panic("DATABASE_URI or DATABASE_NAME are not found in the environment")
	}

	config = Config{
		DatabaseURI:  databaseURI,
		DatabaseName: databaseName,
		ServerPort:   serverPort,
	}
}
