package app

// Secret is a model for secret
type Secret struct {
	ID      string `bson:"_id"`
	Message string `bson:"message"`
	Hash    []byte `bson:"hash"`
}
