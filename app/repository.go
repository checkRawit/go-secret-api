package app

import (
	"context"
	"errors"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// SecretRepositorier is a interface for SecretRepository
type SecretRepositorier interface {
	Create(secret Secret) error
	Read(id string) (Secret, error)
}

// MongodbSecretRepository is a secret repository by mongodb
type MongodbSecretRepository struct {
	client     *mongo.Client
	collection *mongo.Collection
	timeout    time.Duration
}

// NewMongodbSecretRepository create a MongodbSecretRepository
func NewMongodbSecretRepository(connection string, databaseName string, collectionName string) (MongodbSecretRepository, error) {
	defaultTimeout := 10 * time.Second

	// Connect to server
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connection))
	if err != nil {
		return MongodbSecretRepository{}, err
	}
	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		return MongodbSecretRepository{}, err
	}

	return MongodbSecretRepository{client, client.Database(databaseName).Collection(collectionName), defaultTimeout}, nil
}

// Close is a function to close a client in MongodbSecretRepository
func (r MongodbSecretRepository) Close() error {
	ctx, cancel := context.WithTimeout(context.Background(), r.timeout)
	defer cancel()
	if err := r.client.Disconnect(ctx); err != nil {
		return err
	}

	return nil
}

func (r MongodbSecretRepository) Create(secret Secret) error {
	ctx, cancel := context.WithTimeout(context.Background(), r.timeout)
	defer cancel()
	if _, err := r.collection.InsertOne(ctx, secret); err != nil {
		return err
	}

	return nil
}

func (r MongodbSecretRepository) Read(id string) (Secret, error) {
	ctx, cancel := context.WithTimeout(context.Background(), r.timeout)
	defer cancel()
	secret := Secret{}
	if err := r.collection.FindOne(ctx, bson.M{"_id": id}).Decode(&secret); err != nil {
		return Secret{}, err
	}

	return secret, nil
}

// InMemSecretRepository is a In-memory database for SecretRepository
type InMemSecretRepository struct {
	secret map[string]struct {
		Message string
		Hash    []byte
	}
}

// NewInMemSecretRepository create a InMemSecretRepository
func NewInMemSecretRepository() InMemSecretRepository {
	return InMemSecretRepository{secret: make(map[string]struct {
		Message string
		Hash    []byte
	})}
}

// Create is a function that store Secret
func (r InMemSecretRepository) Create(secret Secret) error {
	if _, ok := r.secret[secret.ID]; ok {
		return errors.New("InMemSecretRepository: Create: duplicate ID")
	}

	r.secret[secret.ID] = struct {
		Message string
		Hash    []byte
	}{secret.Message, secret.Hash}
	return nil
}

// Read is a function that return a secret by id
func (r InMemSecretRepository) Read(id string) (Secret, error) {
	secret, ok := r.secret[id]
	if !ok {
		return Secret{}, errors.New("InMemSecretRepository: Read: secret not found")
	}

	return Secret{ID: id, Message: secret.Message, Hash: secret.Hash}, nil
}
