package app

import (
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

type SecretIDServicer interface {
	Generate() string
}

type HashServicer interface {
	Hash(password string) ([]byte, error)
	Verify(password string, hash []byte) bool
}

type UUIDSecretIDService struct{}

func (s UUIDSecretIDService) Generate() string {
	return uuid.New().String()
}

type BcryptHashService struct{}

func (BcryptHashService) Hash(password string) ([]byte, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		return nil, err
	}

	return hash, nil
}

func (BcryptHashService) Verify(password string, hash []byte) bool {
	if err := bcrypt.CompareHashAndPassword(hash, []byte(password)); err != nil {
		return false
	}
	return true
}
