package app

import (
	"fmt"
)

// Usecase is a usecase
type Usecase struct {
	secretRepository    SecretRepositorier
	uuidSecretIDService SecretIDServicer
	hashService         HashServicer
}

// NewUsecase create a usecase and inject dependencies
func NewUsecase(secretRepository SecretRepositorier, secretIDService SecretIDServicer, hashService HashServicer) Usecase {
	return Usecase{secretRepository, secretIDService, hashService}
}

// GetSecret is a usecase that user want to receive a secret message by specific secret ID
func (u Usecase) GetSecret(id string, password string) (string, error) {
	secret, err := u.secretRepository.Read(id)
	if err != nil {
		return "", fmt.Errorf("Usecase: GetSecret: cannot get secret from repository %v", err)
	}

	// Verify password
	isValidPassword := u.hashService.Verify(password, secret.Hash)
	if !isValidPassword {
		return "", fmt.Errorf("Usecase: GetSecret: password is not valid %v", err)
	}

	return secret.Message, nil
}

// StoreSecret is a usecase that user store a secret message and receive back the secret ID
func (u Usecase) StoreSecret(message string, password string, id string) (string, error) {
	// Generate the ID if it not specific
	if id == "" {
		id = u.uuidSecretIDService.Generate()
	}

	hashedPassword, err := u.hashService.Hash(password)
	if err != nil {
		return "", fmt.Errorf("Usecase: StoreSecret: cannot hash password %v", err)
	}

	if err := u.secretRepository.Create(Secret{ID: id, Hash: hashedPassword, Message: message}); err != nil {
		return "", fmt.Errorf("Usecase: StoreSecret: cannot create secret into repository %v", err)
	}

	return id, nil
}
